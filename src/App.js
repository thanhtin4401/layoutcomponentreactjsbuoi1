import logo from './logo.svg';
import './App.css';
import NavbarComponent from './Component/NavbarComponent';
import BannerComponent from './Component/BannerComponent';
import ItemComponent from './Component/ItemComponent';
import './Style/Style.css'
import FooterComponent from './Component/FooterComponent';
function App() {
  return (
    <div className="container">

      <NavbarComponent />
      <BannerComponent />
      <section className="pt-4">
        <div className="conatainer row px-lg-5">
          <ItemComponent />
          <ItemComponent />
          <ItemComponent />
          <ItemComponent />
        </div>
      </section>
      <FooterComponent />
    </div>
  );
}

export default App;
